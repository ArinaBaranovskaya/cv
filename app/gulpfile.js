//содержит инструкции

const { //получение всех констант
    src,
    dest,
    parallel,
    series,
    watch
} = require('gulp');

// Load plugins - подключение пакетов

const cssnano = require('gulp-cssnano');
const changed = require('gulp-changed');
const browsersync = require('browser-sync').create();
const imagemin = require('gulp-imagemin');
const clean = require('gulp-clean');



function clear() { //очищает папку build, если мы хотим что-то изменить
    return src('./build/*', {
        read: false
    })
        .pipe(clean());
}

// CSS 

function css() {
    const source = './src/css/*';

    return src(source)
        .pipe(changed(source))//проверка, есть ли изменения
        .pipe(cssnano())//если есть изменения, то прогоняются они через пакет cssnano
        .pipe(dest('./build/css/'))//сохранение
        .pipe(browsersync.stream());//перезагрузка браузера
}

function js() {
    const source = './src/js/*';

    return src(source)
        .pipe(changed(source))
        .pipe(dest('./build/js/'))
        .pipe(browsersync.stream());
}

// Optimize images

function img() {
    return src('./src/images/*')//отслеживание всего, что находится в папке images
        .pipe(imagemin())//оптимизируется, imagemin-специальный пакет для gulp, который позволяет сжимать картинки при необходимости
        .pipe(dest('./build/images'));//копирование в папку ./build/images
}

// html

function html() {
    return src('./src/*.html')//получает из src все html файлы
        .pipe(dest('./build/'))//копирование(pipe) назначений(destination) в папку build
        .pipe(browsersync.stream());//перезагрузка браузера
}

// Watch files

function watchFiles() {
    watch('./src/css/*', css);
    watch('./src/js/*', js);
    watch('./src/*.html', html);
    watch('./src/images/*', img);
}

// BrowserSync

function browserSync() {
    browsersync.init({
        server: {
            baseDir: './build'
        },
        port: 3000
    });
}

exports.watch = parallel(watchFiles, browserSync);
exports.default = series(clear, parallel(html, js, css, img));//сценарий default - очищает папку build, затем запускает html, css, img (для запуска в консоле вводится gulp)